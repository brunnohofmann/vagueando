package com.ifpb.brunno.vagueando.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 01/07/16.
 */
public class OptionDAO implements DAO<Option> {
    private BancoHelper banco;
    private static final String TABELA = "options";

    public OptionDAO(Context context) {
        this.banco = new BancoHelper(context);
    }

    @Override
    public void inserir(Option newOp) {
        ContentValues cv = new ContentValues();
        cv.put("option", newOp.getOption());
        cv.put("value", newOp.getValue());
        this.banco.getWritableDatabase().insert(TABELA, null, cv);
    }

    @Override
    public void atualizar(Option obj) {
    }

    @Override
    public void remover(int id) {
        String[] where = {Integer.toString(id)};
        this.banco.getWritableDatabase().delete(TABELA, "id = ?", where);
    }

    @Override
    public void remover(Option obj) {
        this.remover(obj.getId());
    }

    @Override
    public Option get(int id) {
        return null;
    }

    @Override
    public List<Option> get() {
        String[] colunas = {"id", "option", "value"};
        List<Option> lista = new ArrayList<Option>();

        Cursor c = this.banco.getReadableDatabase().query(TABELA, colunas, null, null, null, null, "option");
        if (c.getCount() > 0){
            c.moveToFirst();
            do{
                Option p = new Option();
                p.setId(c.getInt(c.getColumnIndex(colunas[0])));
                p.setOption(c.getString(1));
                lista.add(p);
            }while (c.moveToNext());
        }

        return lista;
    }
}
