package com.ifpb.brunno.vagueando.DAO;

import java.io.Serializable;

/**
 * Created by Brunno on 16-Jul-16.
 */
public class Curso implements Serializable{
    int id;
    String nome_curso;

    @Override
    public String toString() {
        return "Curso{" +
                "id=" + id +
                ", nome_curso='" + nome_curso + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome_curso() {
        return nome_curso;
    }

    public void setNome_curso(String nome_curso) {
        this.nome_curso = nome_curso;
    }
}
