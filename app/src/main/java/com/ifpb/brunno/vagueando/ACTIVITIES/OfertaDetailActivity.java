package com.ifpb.brunno.vagueando.ACTIVITIES;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TextView;

import com.ifpb.brunno.vagueando.DAO.Oferta;
import com.ifpb.brunno.vagueando.R;

public class OfertaDetailActivity extends AppCompatActivity {
    TextView ofertaDetailDescription, ofertaDetailBenefits, ofertaDetailSchedule,ofertaDetailSalary, ofertaDetailTurn, ofertaDetailinfs;
    TextView ofertaEmpresaNome, ofertaEmpresaPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Oferta oferta = (Oferta) getIntent().getSerializableExtra("OFERTA");
        setTitle(oferta.getVaga());
        setContentView(R.layout.activity_oferta_detail);
        this.instanciaInterface();

        //configurando as tabs
        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();
            //tab oferta
            TabHost.TabSpec spec = host.newTabSpec("Vaga");
            spec.setContent(R.id.tabOferta);
            spec.setIndicator("Vaga");
            host.addTab(spec);
            //tab Empresa
            spec = host.newTabSpec("Empresa");
            spec.setContent(R.id.tabEmpresa);
            spec.setIndicator("Empresa");
            host.addTab(spec);



        //inserindo dados na tela
        this.ofertaDetailDescription.setText(oferta.getDescricao());
        this.ofertaDetailBenefits.setText(oferta.getBeneficios());
        this.ofertaDetailSchedule.setText(oferta.getCarga_horaria());
        this.ofertaDetailSalary.setText(oferta.getBolsa());
        this.ofertaDetailTurn.setText(oferta.getTurno());
        this.ofertaDetailinfs.setText(oferta.getInf_adicionais());
        this.ofertaEmpresaNome.setText(oferta.getEmpresa().getNome());
        this.ofertaEmpresaPhone.setText(oferta.getEmpresa().getTelefone());
    }

    private void instanciaInterface(){
        this.ofertaDetailDescription = (TextView) findViewById(R.id.ofertaDetailDescription);
        this.ofertaDetailBenefits = (TextView) findViewById(R.id.ofertaDetailBenefits);
        this.ofertaDetailSchedule = (TextView) findViewById(R.id.ofertaDetailSchedule);
        this.ofertaDetailSalary = (TextView) findViewById(R.id.ofertaDetailSalary);
        this.ofertaDetailTurn = (TextView) findViewById(R.id.ofertaDetailTurn);
        this.ofertaDetailinfs = (TextView) findViewById(R.id.ofertaDetailinfs);
        this.ofertaEmpresaNome = (TextView) findViewById(R.id.ofertaEmpresaNome);
        this.ofertaEmpresaPhone = (TextView) findViewById(R.id.ofertaEmpresaPhone);
    }
}
