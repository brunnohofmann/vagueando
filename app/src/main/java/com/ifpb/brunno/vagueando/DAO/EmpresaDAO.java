package com.ifpb.brunno.vagueando.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 01/07/16.
 */
public class EmpresaDAO implements DAO<Empresa> {
    private BancoHelper banco;
    private static final String TABELA = "empresa";

    public EmpresaDAO(Context context) {
        this.banco = new BancoHelper(context);
    }

    @Override
    public void inserir(Empresa newOp) {
        ContentValues cv = new ContentValues();
        cv.put("nome", newOp.getNome());
        cv.put("endereco", newOp.getEndereco());
        cv.put("telefone", newOp.getTelefone());
        this.banco.getWritableDatabase().insert(TABELA, null, cv);
    }

    @Override
    public void atualizar(Empresa obj) {
    }

    @Override
    public void remover(int id) {
        String[] where = {Integer.toString(id)};
        this.banco.getWritableDatabase().delete(TABELA, "id = ?", where);
    }

    @Override
    public void remover(Empresa obj) {
        this.remover(obj.getId());
    }

    @Override
    public Empresa get(int id) {
        return null;
    }

    @Override
    public List<Empresa> get() {
        String[] colunas = {"id", "nome", "endereco", "telefone"};
        List<Empresa> lista = new ArrayList<Empresa>();

        Cursor c = this.banco.getReadableDatabase().query(TABELA, colunas, null, null, null, null, "nome");
        if (c.getCount() > 0){
            c.moveToFirst();
            do{
                Empresa p = new Empresa();
                p.setId(c.getInt(c.getColumnIndex(colunas[0])));
                p.setNome(c.getString(1));
                lista.add(p);
            }while (c.moveToNext());
        }

        return lista;
    }
}
