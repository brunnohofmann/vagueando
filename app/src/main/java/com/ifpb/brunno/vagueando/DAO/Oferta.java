package com.ifpb.brunno.vagueando.DAO;

import java.io.Serializable;

/**
 * Created by Brunno on 16-Jul-16.
 */
public class Oferta implements Serializable{
    private int id;
    private String vaga, descricao, carga_horaria, bolsa, beneficios, inf_adicionais, turno;
    private Empresa empresa;
    private Curso curso;


    public String getBolsa() {
        return bolsa;
    }

    public void setBolsa(String bolsa) {
        this.bolsa = bolsa;
    }

    public String getCarga_horaria() {
        return carga_horaria;
    }

    public void setCarga_horaria(String carga_horaria) {
        this.carga_horaria = carga_horaria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getVaga() {
        return vaga;
    }

    public void setVaga(String vaga) {
        this.vaga = vaga;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBeneficios() {
        return beneficios;
    }

    public void setBeneficios(String beneficios) {
        this.beneficios = beneficios;
    }

    public String getInf_adicionais() {
        return inf_adicionais;
    }

    public void setInf_adicionais(String inf_adicionais) {
        this.inf_adicionais = inf_adicionais;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    @Override
    public String toString() {
        return "Oferta{" +
                "id=" + id +
                ", curso=" + curso +
                ", vaga='" + vaga + '\'' +
                ", descricao='" + descricao + '\'' +
                ", carga_horaria='" + carga_horaria + '\'' +
                ", bolsa='" + bolsa + '\'' +
                ", beneficios='" + beneficios + '\'' +
                ", inf_adicionais='" + inf_adicionais + '\'' +
                ", turno='" + turno + '\'' +
                ", empresa=" + empresa +
                '}';
    }
}
