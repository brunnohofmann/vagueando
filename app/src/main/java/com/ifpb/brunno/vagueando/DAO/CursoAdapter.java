package com.ifpb.brunno.vagueando.DAO;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ifpb.brunno.vagueando.R;

import java.util.List;

/**
 * Created by Brunno on 16-Jul-16.
 */
public class CursoAdapter extends BaseAdapter {

    private List<Curso> lista;
    private Context context;

    public CursoAdapter(List<Curso> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Curso p = this.lista.get(position);

        if (convertView == null){
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.curso_layout, null);
        }else view = convertView;

        TextView tv = (TextView) view.findViewById(R.id.tvNomeCursoLayout);
        tv.setText(p.getNome_curso());

        return view;
    }
}
