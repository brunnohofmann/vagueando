package com.ifpb.brunno.vagueando.ACTIVITIES;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.ifpb.brunno.vagueando.DAO.EmpresaAdapter;
import com.ifpb.brunno.vagueando.DAO.EmpresaDAO;
import com.ifpb.brunno.vagueando.R;

/**
 * Created by Brunno on 18-Jul-16.
 */
public class EmpresasActivity extends AppCompatActivity {
    private EmpresaDAO empresaDAO;

    private ListView lvEmpresas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);

        this.instaciaInterface();

        this.empresaDAO = new EmpresaDAO(this);

        EmpresaAdapter adapterEmpresas = new EmpresaAdapter(this.empresaDAO.get(), this);
        this.lvEmpresas.setAdapter(adapterEmpresas);

        this.atualizaEmpresasAdapter();
    }

    private void instaciaInterface(){
        this.lvEmpresas = (ListView) findViewById(R.id.lvEmpresas);
    }

    private void atualizaEmpresasAdapter(){
        this.lvEmpresas.setAdapter(new EmpresaAdapter(this.empresaDAO.get(), this));
    }
}
