package com.ifpb.brunno.vagueando;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ifpb.brunno.vagueando.ACTIVITIES.OfertaDetailActivity;
import com.ifpb.brunno.vagueando.ACTIVITIES.CursosActivity;
import com.ifpb.brunno.vagueando.ACTIVITIES.EmpresasActivity;
import com.ifpb.brunno.vagueando.DAO.Curso;
import com.ifpb.brunno.vagueando.DAO.CursoDAO;
import com.ifpb.brunno.vagueando.DAO.Empresa;
import com.ifpb.brunno.vagueando.DAO.EmpresaDAO;
import com.ifpb.brunno.vagueando.DAO.Oferta;
import com.ifpb.brunno.vagueando.DAO.OfertaAdapter;
import com.ifpb.brunno.vagueando.DAO.OfertaDAO;
import com.ifpb.brunno.vagueando.DAO.OptionDAO;

public class MainActivity extends AppCompatActivity {

    private OptionDAO options;

    private EmpresaDAO empresaDao;
    private OfertaDAO ofertaDao;
    private CursoDAO cursoDAO;

    private ListView lvEmpresas;
    private ListView lvOfertas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Configuração do Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.instaciaInterface();
        this.defineListeners();

        this.empresaDao = new EmpresaDAO(this);
        this.ofertaDao = new OfertaDAO(this);
        this.cursoDAO = new CursoDAO(this);

        OfertaAdapter adapterOfertas = new OfertaAdapter(this.ofertaDao.get(), this);
        this.lvOfertas.setAdapter(adapterOfertas);

        //inserindo registros o
        this.inserts();
        this.atualizaOfertasAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cursos) {
            Intent it = new Intent(MainActivity.this, CursosActivity.class);
            startActivity(it);
        }
        if (id == R.id.action_empresas) {
            Intent it = new Intent(MainActivity.this, EmpresasActivity.class);
            startActivity(it);
        }

        return super.onOptionsItemSelected(item);
    }

    private void atualizaOfertasAdapter(){
        this.lvOfertas.setAdapter(new OfertaAdapter(this.ofertaDao.get(), this));
    }

    private void instaciaInterface(){
        this.lvOfertas = (ListView) findViewById(R.id.lvOfertas);
    }
    private void defineListeners(){
        this.lvOfertas.setOnItemClickListener(new OfertaClickItemListener());
    }

    private void inserts(){
        Empresa emp = new Empresa();
        emp.setId(1);
        emp.setNome("Dom Comunicacao");
        emp.setEndereco("Av. Juiz Gama e MElo, 72");
        emp.setTelefone("8332252742");
        this.empresaDao.inserir(emp);

        Curso cur = new Curso();
        cur.setId(1);
        cur.setNome_curso("Sistemas para Internet");
        this.cursoDAO.inserir(cur);

        Oferta of = new Oferta();
        of.setVaga("Analista de Dados");
        of.setEmpresa(emp);
        of.setCurso(cur);
        of.setBolsa("600,00");
        of.setCarga_horaria("40 horas");
        of.setDescricao("Gerenciar fila de demandas de cliente, e organizar capacidade de atendimento de recursos das áreas clientes. Desenvolver rotinas de carga e integrações de dados SQL / SAS. Analisar problemas de regras de negócio sugerindo manutenções nos sistemas de produção.\n" +
                "Conhecimento em linguagem SQL ( ou qualquer GBD relacionados). Graduação / Tecnólogo ou certificação exemplo MCSE, Oracle ou outra. Experiência Manipulação de informações, conhecimento em modelagem de dados e Experiência em ambientes de produção.");
        of.setBeneficios("Assistência Médica / Medicina em grupo, Assistência Odontológica, Tíquete-refeição, Vale-transporte");
        of.setTurno("Tarde");
        of.setInf_adicionais("Disponibilidade para viagens");

        this.ofertaDao.inserir(of);

        //Log.i("Vagueando", MainActivity.this.cursoDAO.get().toString());
    }


    ////LISTENERSSSSSSSSSSSSS
    private class OfertaClickItemListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Oferta ofer = (Oferta) adapterView.getAdapter().getItem(i);

            Intent detalhesOferta = new Intent(MainActivity.this, OfertaDetailActivity.class);
            detalhesOferta.putExtra("OFERTA", ofer);
            startActivity(detalhesOferta);
        }
    }
}
