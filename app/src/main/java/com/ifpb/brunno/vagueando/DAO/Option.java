package com.ifpb.brunno.vagueando.DAO;

/**
 * Created by Brunno on 15-Jul-16.
 */
public class Option {
    private int id;
    private String option, value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
