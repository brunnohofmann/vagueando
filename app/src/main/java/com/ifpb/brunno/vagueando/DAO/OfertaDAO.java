package com.ifpb.brunno.vagueando.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brunno on 16-Jul-16.
 */
public class OfertaDAO implements DAO<Oferta> {
    private BancoHelper banco;
    private static final String TABELA = "ofertas";
    private static final String TABELAEMPRESA = "empresa";
    private static final String TABELACURSO = "cursos";

    public OfertaDAO(Context context) {
        this.banco = new BancoHelper(context);
    }

    @Override
    public void inserir(Oferta newOp) {
        ContentValues cv = new ContentValues();
        cv.put("vaga", newOp.getVaga());
        cv.put("descricao", newOp.getDescricao());
        cv.put("carga_horaria", newOp.getCarga_horaria());
        cv.put("bolsa", newOp.getBolsa());
        cv.put("idempresa", newOp.getEmpresa().getId());
        cv.put("idcurso", newOp.getCurso().getId());
        cv.put("beneficios", newOp.getBeneficios());
        cv.put("inf_adicionais", newOp.getInf_adicionais());
        cv.put("turno", newOp.getTurno());
        this.banco.getWritableDatabase().insert(TABELA, null, cv);
    }

    @Override
    public void atualizar(Oferta obj) {
    }

    @Override
    public void remover(int id) {
        String[] where = {Integer.toString(id)};
        this.banco.getWritableDatabase().delete(TABELA, "id_oferta = ?", where);
    }

    @Override
    public void remover(Oferta obj) {
        this.remover(obj.getId());
    }

    @Override
    public Oferta get(int id) {
        return null;
    }

    public Empresa getEmpresa(int id){
        //o ideal seria coletar essa informação do DAOEMPRESA, mas não pode ser criado aqui pois afirma que o contexto da OFERTADAO não pode ser enviado
        //essa função então, le a empresa do banco e cria o objeto empresa, atrelando à oferta
        String where = " id = " + Integer.toString(id);
        String[] colunas = {"id", "nome", "endereco", "telefone"};
        Empresa empresa = new Empresa();
        Cursor c = this.banco.getReadableDatabase().query(true, TABELAEMPRESA, colunas, where, null, null, null, null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            empresa.setId(c.getInt(0));
            empresa.setNome(c.getString(1));
            empresa.setEndereco(c.getString(2));
            empresa.setTelefone(c.getString(3));
        }
        return empresa;
    }

    public Curso getCurso(int id){
        //o mesmo de getEmpresa
        String where = " id_curso = " + Integer.toString(id);
        String[] colunas = {"id_curso", "nome_curso"};
        Curso curso = new Curso();
        Cursor c = this.banco.getReadableDatabase().query(true, TABELACURSO, colunas, where, null, null, null, null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            curso.setId(c.getInt(0));
            curso.setNome_curso(c.getString(1));
        }
        return curso;
    }

    @Override
    public List<Oferta> get() {
        String[] colunas = {"id_oferta", "vaga", "descricao", "carga_horaria", "bolsa", "idempresa", "idcurso", "beneficios", "inf_adicionais", "turno"};
        List<Oferta> lista = new ArrayList<Oferta>();


        Cursor c = this.banco.getReadableDatabase().query(TABELA, colunas, null, null, null, null, "vaga");
        if (c.getCount() > 0){
            c.moveToFirst();
            do{
                Oferta p = new Oferta();
                p.setId(c.getInt(c.getColumnIndex(colunas[0])));
                p.setVaga(c.getString(1));
                p.setDescricao(c.getString(2));
                p.setCarga_horaria(c.getString(3));
                p.setBolsa(c.getString(4));
                p.setEmpresa(this.getEmpresa(c.getInt(5)));
                p.setCurso(this.getCurso(c.getInt(6)));
                p.setBeneficios(c.getString(7));
                p.setInf_adicionais(c.getString(8));
                p.setTurno(c.getString(9));
                lista.add(p);

            }while (c.moveToNext());
        }

        return lista;
    }
}
