package com.ifpb.brunno.vagueando.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Brunno on 14-Jul-16.
 */
public class BancoHelper extends SQLiteOpenHelper {

    private static final String BANCO = "vagueando.db";
    private static final int VERSAO = 1;

    public BancoHelper(Context context) {
        super(context, BANCO, null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String tabelaEmpresa = "create table empresa(" +
                "id integer primary key autoincrement not null," +
                "nome string," +
                "endereco string," +
                "telefone string" +
                ");";
        db.execSQL(tabelaEmpresa);
        Log.i("VAGUEANDO", "Tabela Empresa criada.");
        String tabelaOfertas = "create table ofertas  (" +
                "id_oferta INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "vaga STRING," +
                "descricao STRING," +
                "carga_horaria STRING," +
                "bolsa STRING, " +
                "beneficios STRING," +
                "inf_adicionais STRING," +
                "turno STRING," +
                "idempresa INTEGER," +
                "idcurso INTEGER" +
                ");";
        db.execSQL(tabelaOfertas);
        Log.i("VAGUEANDO", "Tabela Oferta criada.");

        String tabelaCurso = "create table cursos  (" +
                "id_curso INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "nome_curso STRING" +
                ");";
        db.execSQL(tabelaCurso);
        Log.i("VAGUEANDO", "Tabela Curso criada.");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
