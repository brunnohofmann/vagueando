package com.ifpb.brunno.vagueando.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brunno on 16-Jul-16.
 */
public class CursoDAO implements DAO<Curso> {

    private BancoHelper banco;
    private static final String TABELA = "cursos";

    public CursoDAO(Context context) {
        this.banco = new BancoHelper(context);
    }

    @Override
    public void inserir(Curso newOp) {
        ContentValues cv = new ContentValues();
        cv.put("nome_curso", newOp.getNome_curso());
        this.banco.getWritableDatabase().insert(TABELA, null, cv);
    }

    @Override
    public void atualizar(Curso obj) {
    }

    @Override
    public void remover(int id) {
        String[] where = {Integer.toString(id)};
        this.banco.getWritableDatabase().delete(TABELA, "id_curso = ?", where);
    }

    @Override
    public void remover(Curso obj) {
        this.remover(obj.getId());
    }

    @Override
    public Curso get(int id) {
        return null;
    }

    @Override
    public List<Curso> get() {
        String[] colunas = {"id_curso", "nome_curso"};
        List<Curso> lista = new ArrayList<Curso>();

        Cursor c = this.banco.getReadableDatabase().query(TABELA, colunas, null, null, null, null, "nome_curso");
        if (c.getCount() > 0){
            c.moveToFirst();
            do{
                Curso p = new Curso();
                p.setId(c.getInt(c.getColumnIndex(colunas[0])));
                p.setNome_curso(c.getString(1));
                lista.add(p);
            }while (c.moveToNext());
        }
        return lista;
    }
}
