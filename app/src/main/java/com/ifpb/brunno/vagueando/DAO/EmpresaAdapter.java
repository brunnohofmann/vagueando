package com.ifpb.brunno.vagueando.DAO;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ifpb.brunno.vagueando.R;

import java.util.List;

/**
 * Created by Brunno on 15-Jul-16.
 */
public class EmpresaAdapter extends BaseAdapter {
    private List<Empresa> lista;
    private Context context;

    public EmpresaAdapter(List<Empresa> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }



    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Empresa p = this.lista.get(position);

        if (convertView == null){
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.empresa_layout, null);
        }else view = convertView;

        TextView tv = (TextView) view.findViewById(R.id.tvNomeEmpresaLayout);
        tv.setText(p.getNome());

        return view;
    }
}
