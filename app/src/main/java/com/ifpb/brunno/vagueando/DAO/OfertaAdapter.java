package com.ifpb.brunno.vagueando.DAO;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ifpb.brunno.vagueando.R;

import java.util.List;

/**
 * Created by Brunno on 16-Jul-16.
 */
public class OfertaAdapter extends BaseAdapter {
    private List<Oferta> lista;
    private Context context;

    public OfertaAdapter(List<Oferta> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Oferta p = this.lista.get(position);

        if (convertView == null){
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.oferta_layout, null);
        }else view = convertView;

        TextView tv = (TextView) view.findViewById(R.id.tvNomeOfertaLayout);
        TextView tvFaixaSalario = (TextView) view.findViewById(R.id.tvFaixaSalatiOfertaLayout);
        tv.setText(p.getVaga());
        tvFaixaSalario.setText("R$ "+p.getBolsa());

        return view;
    }
}
