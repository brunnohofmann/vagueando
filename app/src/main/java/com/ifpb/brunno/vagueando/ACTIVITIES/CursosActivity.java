package com.ifpb.brunno.vagueando.ACTIVITIES;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.ifpb.brunno.vagueando.DAO.CursoAdapter;
import com.ifpb.brunno.vagueando.DAO.CursoDAO;
import com.ifpb.brunno.vagueando.DAO.OfertaAdapter;
import com.ifpb.brunno.vagueando.R;

/**
 * Created by Brunno on 18-Jul-16.
 */
public class CursosActivity extends AppCompatActivity {
    private CursoDAO cursoDAO;

    private ListView lvCursos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cursos);


        this.instaciaInterface();

        this.cursoDAO = new CursoDAO(this);

        CursoAdapter adapterCursos = new CursoAdapter(this.cursoDAO.get(), this);
        this.lvCursos.setAdapter(adapterCursos);

        this.atualizaCursosAdapter();
    }

    private void instaciaInterface(){
        this.lvCursos = (ListView) findViewById(R.id.lvCursos);
    }

    private void atualizaCursosAdapter(){
        this.lvCursos.setAdapter(new CursoAdapter(this.cursoDAO.get(), this));
    }
}

